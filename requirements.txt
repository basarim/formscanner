cycler==0.10.0
decorator==4.4.1
entrypoints==0.3
flake8==3.7.9
imageio==2.6.1
kiwisolver==1.1.0
matplotlib==3.1.2
mccabe==0.6.1
networkx==2.4
numpy==1.17.4
opencv-python==4.1.2.30
Pillow==6.2.1
pkg-resources==0.0.0
pycodestyle==2.5.0
pyflakes==2.1.1
pyparsing==2.4.5
python-dateutil==2.8.1
PyWavelets==1.1.1
scikit-image==0.16.2
scipy==1.4.1
six==1.13.0
