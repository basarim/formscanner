from skimage.filters import threshold_yen
import numpy as np
import cv2 as cv
import os


class Image:
    """Class for basic image operations. Initialize with path to image file.

    Class for basic Image operations using Python Opencv.
    This package is developed for the needs of BASARIM app.


    Attributes:
    ------
    image : numpy.ndarray
        original image read from path

    resized : numpy.ndarray
        resized image. initially none, created after calling resize() method

    grayscaled : numpy.ndarray
        grayscaled image. initially none, created after calling grayscale()
        method

    corners : numpy.ndarray
        edge locations of the document in the image

    warped : numpy.ndarray
        transformed image after finding document corners. initially None,
        created after calling transform method.

    scanned : numpy.ndarray
        Local thresholded image. Applied after warping the document. Initially
        None, created adter calling scan method.


    Methods:
    --------
    resize(ratio -> int) -> numpy.ndarray
        resized the original image, saves and returns.

    save_contours() -> numpy.ndarray
        saves the contours on the image

    find_corners_of_document(
        kernel_l -> int,
        kernel_h -> int,
        canny_l -> int,
        canny_h -> int,
    ) -> numpy.ndarray
        returns the corner points of the document in the image.

    transform() -> numpy.ndarray
        returns and saves the transforms image of the document in the original
        image. Call it after find_corners_of_document method.

    threshold() -> numpy.ndarray
        apply local threshold with yen method. return and save the scanned
        image.

    scan() -> numpy.ndarray
        apply all operations for scanning at once.

    show(caption -> str, image_choice -> str) -> None
        show desired image.
    """

    def __init__(self, path: str) -> None:

        if not os.path.exists(path):
            raise FileNotFoundError("Image not found in the given path.")

        self.image = cv.imread(path, cv.IMREAD_UNCHANGED)  # read image at path

        # these variables will be created after method calls
        self.resized = None
        self.warped = None
        self.corners = None
        self.scanned = None
        self._grayscaled = None

    def resize(self, ratio=-1) -> np.ndarray:
        """ Resizes the original image with given ratio. Saves and returns the
        resized image. Saves resizing ratio for later use. Used without
        ratio parameter internally."""

        if ratio == -1:
            if self.image.shape[1] > 1000:
                self._resizing_ratio = 100 / (self.image.shape[1] / 1000)
            else:
                self._resizing_ratio = 100  # don't need to scale the image
                self.resized = self.image
                return self.resized
        else:
            self._resizing_ratio = ratio

        # width of new image
        width = int(self.image.shape[1] * self._resizing_ratio / 100)

        # height of new image
        height = int(self.image.shape[0] * self._resizing_ratio / 100)

        dim = (width, height)  # combine to get dimensions of image
        self.resized = cv.resize(self.image, dim)  # save resized image

        return self.resized

    def _save_grayscaled_image(self):
        """Save a grayscaled version of resized image."""

        if self.resized is None:
            self.resize()  # apply resizing if not applied.

        # save and return for later use
        self._grayscaled = cv.cvtColor(self.resized, cv.COLOR_BGR2GRAY)

    def _save_blurred_image(self, kernel_size: tuple) -> np.ndarray:
        """Private method that saves blurred image with given Kernel size and
        Gaussian algorithm."""

        self._blurred = cv.GaussianBlur(self._grayscaled, kernel_size, 0)

    def _save_canny_image(self, low=100, high=200):
        """
        Apply canny algorithm to blurred image.
        """
        self._edges = cv.Canny(self._blurred, low, high)

    def save_contours(self) -> np.ndarray:
        """Get contours of edged image."""

        self._contours, hierarchy = cv.findContours(
            self._edges, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE
        )

    def _approx_poly(self, c):
        closed = True
        corners = epsilon = 0
        while corners != 4:
            approx_curve = cv.approxPolyDP(c, epsilon, closed)
            epsilon += 10
            corners = approx_curve.shape[0]
            if epsilon == 100:
                return None
        return approx_curve

    def _order_corners(self, pts: np.ndarray) -> np.ndarray:
        """Order the corner points of document."""

        ordered = np.zeros((4, 2), dtype="float32")
        pts = np.reshape(pts, (4, 2))
        s = pts.sum(axis=1)

        ordered[0] = pts[np.argmin(s)]  # first point has the least sum
        ordered[2] = pts[np.argmax(s)]  # third point has the maximum sum

        diff = np.diff(pts, axis=1)
        ordered[1] = pts[np.argmin(diff)]
        ordered[3] = pts[np.argmax(diff)]

        return ordered

    def find_corners_of_document(
        self, kernel_l: int = 5, kernel_h: int = 5,
        canny_l: int = 100, canny_h: int = 200
    ) -> np.ndarray:
        """Find the four corner points of the document."""

        #  save grayscaled image if not saved
        if self._grayscaled is None:
            self._save_grayscaled_image()

        #  contour area threshold to be accepted
        CNT_AREA_TH = 0.3 * self._grayscaled.size

        #  height / width ratio can be used as condition
        #  H_W_RATIO = 0.66

        #  apply blur and canny edge detection
        self._save_blurred_image((kernel_l, kernel_h))
        self._save_canny_image(canny_l, canny_h)
        self.save_contours()

        #  find the contour that satisfied coditions
        for c in self._contours:
            approx = self._approx_poly(c)  # approximate egdes to 4 if possible
            #  these conditions may change
            if cv.contourArea(c) > CNT_AREA_TH and approx is not None:
                self.corners = self._order_corners(approx)

        #  try with larger canny threshold interval
        if canny_l != 50 and canny_h != 250 and self.corners is None:
            self.find_corners_of_document(kernel_l, kernel_h, 50, 250)

        #  try with lower gaussian blur kernel sizes
        if kernel_l-2 > 0 and kernel_h-2 > 0 and self.corners is None:
            kernel_l -= 2
            kernel_h -= 2
            self.find_corners_of_document(kernel_l, kernel_h, canny_l, canny_h)

        #  we dont need these images anymore
        del self._grayscaled
        del self._blurred
        del self._edges
        del self._contours

        return self.corners

    def transform(self) -> np.ndarray:
        """Transform and warp the image with given corner points."""

        #  get corner points as seperate variable
        (tl, tr, br, bl) = self.corners

        #  get the maximum width value (top or bottom)
        width_a = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        width_b = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        max_width = max(int(width_a), int(width_b))

        #  get maximum height value (left or right)
        height_a = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        height_b = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        max_height = max(int(height_a), int(height_b))

        #  destination image array
        dst = np.array([
            [0, 0],
            [max_width-1, 0],
            [max_width-1, max_height-1],
            [0, max_height-1]], dtype="float32"
        )

        M = cv.getPerspectiveTransform(self.corners, dst)

        #  apply perspective transformation
        self.warped = cv.warpPerspective(
            self.resized, M, (max_width, max_height)
        )

        return self.warped

    def threshold(self) -> np.ndarray:
        """Apply local threshold to get binary image of the document."""

        #  preprocessing before appyling threshold (grayscale, blur)
        grayscale_warped = cv.cvtColor(self.warped, cv.COLOR_BGR2GRAY)
        blurred_warped = cv.GaussianBlur(grayscale_warped, (5, 5), 0)

        #  calculate local thresholds, apply binarization, save
        th_value = threshold_yen(blurred_warped)
        self.scanned = (blurred_warped > th_value).astype("uint8") * 255

    def scan(self) -> np.ndarray:
        """Apply all operations needed for scanning with default parameters."""
        self.find_corners_of_document()
        self.transform()
        self.threshold()

    def show(self, caption="document image", image_choice="original"):

        if image_choice == "original":
            img = self.image
        elif image_choice == "resized":
            img = self.resized
        elif image_choice == "transformed":
            img = self.warped
        elif image_choice == "scanned":
            img = self.scanned
        else:
            img = None

        cv.imshow(caption, img)
        cv.waitKey(0)
        cv.destroyAllWindows()
