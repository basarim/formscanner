# Class for basic image operations.

Operations needed to scan an image.

* grayscale
* save_contours
* find_corners_of_document
* transform
* threshold
* scan
* show

This package developed as a part of BAŞARIM project.

To do:
* add H_W_RATIO condition to accept corners of document contour
* adjust document image to the frame
